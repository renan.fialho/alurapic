import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { switchMap, tap } from 'rxjs/operators';

import { PhotoComment } from '../../photo/photo-comment';
import { PhotoService } from './../../photo/photo.service';

@Component({
  selector: 'ap-photo-comment',
  templateUrl: './photo-comments.component.html',
  styleUrls: ['./photo-comments.component.scss']
})
export class PhotoCommentsComponent implements OnInit{

  @Input() photoId: number;

  comments$: Observable<PhotoComment[]>;

  commentsForm: FormGroup;

  constructor(private photoService: PhotoService, private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.comments$ = this.photoService.getComments(this.photoId);
    this.commentsForm = this.formBuilder.group({
      comment: ['', Validators.maxLength(300)]
    });
  }

  save() {
    console.log('deu certo');
    const comment = this.commentsForm.get('comment').value as string;
    this.comments$ = this.photoService.addComment(this.photoId, comment)
    .pipe(switchMap(() => this.photoService.getComments(this.photoId)))
    .pipe(tap(() => {
      this.commentsForm.reset();
      alert('Comentário adicionado com sucesso');
    }));
  }


}
