import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoadButtonComponent } from './load-button/load-button.component';
import { PhotoListComponent } from './photo-list.component';
import { PhotosComponent } from './photos/photos.component';
import { FilterByDescription } from './photos/filter-by-description.pipe';
import { PhotoModule } from './../photo/photo.module';
import { CardModule } from './../../shared/component/card/card.module';
import { SearchComponent } from './search/search.component';
import { DarkerOnHoverModule } from 'src/app/shared/directives/darken-on-hover/darken-on-hover.module';

@NgModule({
    declarations: [
        PhotoListComponent,
        PhotosComponent,
        LoadButtonComponent,
        FilterByDescription,
        SearchComponent
    ],
    imports: [
        CommonModule,
        PhotoModule,
        CardModule,
        DarkerOnHoverModule,
        RouterModule
    ]
})
export class PhotoListModule {}
