import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ServerLogService } from './server-log.service';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ErrorHandler, Injectable, Injector, Type } from '@angular/core';

import * as StackTrace from 'stacktrace-js';
import { UserService } from './../../core/user/user.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) {}

  handleError(error: any): void {
    const location = this.injector.get(LocationStrategy as Type<LocationStrategy>);
    const userService = this.injector.get(UserService as Type<UserService>);
    const serverLogService = this.injector.get(ServerLogService as Type<ServerLogService>);
    const router = this.injector.get(Router as Type<Router>);

    const url = location instanceof PathLocationStrategy ? location.path() : '';

    const message = error.message ? error.message : error.toString();

    if (environment.production) router.navigate(['/error']);

    StackTrace.fromError(error)
    .then(stackFrames => {
      const stackAsString = stackFrames.map(sf => sf.toString()).join('/n');
      console.log(stackAsString);

      serverLogService.log(
        {
          message,
          url,
          userName: userService.getUserName(),
          stack: stackAsString
        }
      ).subscribe(
        () => console.log('Error loged server'),
        err => {
          console.log(err);
          console.log('Fail to send error to serve');
        }
      );
    })
  }

}
