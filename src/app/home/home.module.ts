import { SignUpService } from './signup/signup.service';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VMessageModule } from '../shared/component/vmessage/vmessage.module';

import { SignInComponent } from './signin/signin.component';
import { HomeRoutingModule } from './home.routing.module';
import { HomeComponent } from './home.component';
import { SignUpCompnent } from './signup/signup.component';

@NgModule({
    declarations: [
        SignInComponent,
        SignUpCompnent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        VMessageModule,
        RouterModule,
        HomeRoutingModule
    ],
    providers: [ SignUpService ]
})
export class HomeModule {}
