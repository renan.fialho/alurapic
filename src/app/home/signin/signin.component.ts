import { PlatformDetectorService } from './../../core/platform-detector/platform-detector.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
    templateUrl: 'signin.component.html'
})
export class SignInComponent implements OnInit {
    loginForm: FormGroup;
    fromUrl: string;

    @ViewChild('userNameInput') userNameInput: ElementRef<HTMLInputElement>

    constructor(private formBuilder: FormBuilder, private authService: AuthService,
    private router: Router, private platformDetectorService: PlatformDetectorService,
    private activatedRoute: ActivatedRoute) {}

    ngOnInit(): void {
      this.activatedRoute.queryParams.subscribe(params => this.fromUrl = params['fromUrl']);

      this.loginForm = this.formBuilder.group({
          userName: ['', Validators.required],
          password: ['', Validators.required]
      });
      this.platformDetectorService.isPlatformBrowser() &&
                    this.userNameInput.nativeElement.focus();
    }

    public login() {
        const username = this.loginForm.get('userName').value;
        const password = this.loginForm.get('password').value;

        this.authService.authenticate(username, password).subscribe(
            () => this.fromUrl ?
              this.router.navigateByUrl(this.fromUrl)
              : this.router.navigate(['user', username])
            ,
            err => {
                this.loginForm.reset();
                this.platformDetectorService.isPlatformBrowser() &&
                    this.userNameInput.nativeElement.focus();
                alert('Invalid user or password')
            }
        );
    }
}
