import { ShowIfLoggedModule } from './../shared/directives/show-if-logged/show-if-logged.module';
import { MenuModule } from './../shared/component/menu/menu.module';
import { LoadingModule } from './../shared/component/loading/loading.module';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from './auth/request.interceptor';
import { HeaderComponent } from './header/header.component';
import { AlertModule } from './../shared/component/alert/alert.module';

@NgModule({
    declarations: [
      HeaderComponent,
      FooterComponent
    ],
    exports: [
      HeaderComponent,
      FooterComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        AlertModule,
        LoadingModule,
        MenuModule,
        ShowIfLoggedModule
    ],
    providers: [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: RequestInterceptor,
        multi: true
      }
    ]
})
export class CoreModule {}
