export class Alert {
  constructor (public readonly typeAlert: AlertType, public readonly message: string) {}
}

export enum AlertType {
  SUCCESS,
  WARNING,
  DANGER,
  INFO
}
