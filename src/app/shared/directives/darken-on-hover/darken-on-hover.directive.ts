import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
    selector: '[apDarkerOnHover]'
})
export class DarkenOnHoverDirective {
    @Input() brightness = '70%'
    constructor(private el: ElementRef, private render: Renderer2) {}

    @HostListener('mouseover')
    darkerOn() {
        this.render.setStyle(this.el.nativeElement, 'filter', `brightness(${this.brightness})`)
    }
    
    @HostListener('mouseleave')
    darkerOff() {
        this.render.setStyle(this.el.nativeElement, 'filter', 'brightness(100%)')
    }
}