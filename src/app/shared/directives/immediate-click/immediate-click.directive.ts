import { Directive, OnInit, ElementRef } from '@angular/core';

import { PlatformDetectorService } from './../../../core/platform-detector/platform-detector.service';

@Directive({
  selector: '[immediateClick]'
})
export class immediateClickDirective implements OnInit{
  constructor(private element: ElementRef<any>, private platformDirectiveService: PlatformDetectorService) {}

  ngOnInit(): void {
    this.platformDirectiveService.isPlatformBrowser &&
      this.element.nativeElement.click();
  }
}
